#!/usr/bin/env bash

# get superuser rights
#sudo su

# remove module if exists
rmmod usb_jet_flash

# clean old compilation output and make module agane
make clean
make all

# remove old version of module from the ../modules/..
rm /lib/modules/2.6.32-38-generic/kernel/usb_jet_flash.ko

# copy new version of module to ../modules/..
cp usb_jet_flash.ko /lib/modules/2.6.32-38-generic/kernel

# install new module
insmod usb_jet_flash.ko

# view it in the list of all installed modules (shows only last ones because our 
# module is always last one installed 
lsmod | head -n 4

# update configuration files to force the system to see our new driver
depmod -a

# like insmod but not work without calling the depmode -a command
modprobe usb_jet_flash

# show kernel output (ATTENTION! this method clears the dmesg ring buffer)
# so if you evaluate dmesg agean after install.sh most probably there will be no output
sudo dmesg -c >> /tmp/dmesg.log; 
tail -n 6 /tmp/dmesg.log

# and finally show our device info (Driver must be equals to: Driver=jet_flash_drive)
cat /sys/kernel/debug/usb/devices | tail -n 10
