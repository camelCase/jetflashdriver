#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>

#define VID 0x8564
#define PID 0x1000

static int jet_flash_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    printk(KERN_INFO "Transcend Jet_flash (%04X:%04X) plugged\n", id->idVendor, id->idProduct);
    return 0;
}

static void jet_flash_disconnect(struct usb_interface *interface)
{
    printk(KERN_INFO "Transcend Jet_flash removed\n");
}

static struct usb_device_id jet_flash_devices_table[] =
        {
                { USB_DEVICE(VID, PID) },
                {} /* Terminating entry */
        };
MODULE_DEVICE_TABLE (usb, jet_flash_devices_table);

static struct usb_driver jet_flash_driver =
        {
                .name = "jet_flash_driver",
                .id_table = jet_flash_devices_table,
                .probe = jet_flash_probe,
                .disconnect = jet_flash_disconnect,
        };

static int __init jet_flash_init(void)
{
    return usb_register(&jet_flash_driver);
}

static void __exit jet_flash_exit(void)
{
    usb_deregister(&jet_flash_driver);
}

module_init(jet_flash_init);
module_exit(jet_flash_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nikita Mityagin");
MODULE_DESCRIPTION("USB Jet Flash Registration Driver");