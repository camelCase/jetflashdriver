CONFIG_MODULE_SIG=n
CONFIG_MODVERSIONS=y

obj-m += usb_jet_flash.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean